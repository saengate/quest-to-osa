#!\user\bin\python

class CalculoDeHora():
	#Constructor de la clase que calcula la hora a partir de los valores dados
	# H representa la Hora
	# M representa los minutos
	def __init__(self, H, M):
		self.M = M
		self.H = H
		if (self.M < 0):
			self.M =(-1*self.M)
			if (self.M > 59):
				self.H += (-1*(self.M // 60))
				if (self.M > 0):
					self.H -= 1
				self.M = 60 - (self.M % 60)
		else:
			if (self.M > 59):
				self.H += (self.M // 60)
				self.M = self.M % 60
		if (self.H < 0):
			self.H =(-1*self.H)
			if(self.H > 23):
				self.H = 24 - (self.H % 24)
			else:
				self.H =24 - self.H	
		else:
			self.H = self.H % 24
		#Imprime el resultado
		strg = ''
		if (self.H < 10):strg = '0' + str(self.H)
		else:strg = strg + str(self.H)
		strg = strg + ':'
		if (self.M < 10):strg = strg + '0' + str(self.M)
		else:strg = strg + str(self.M)
		print strg
	
	#Funcion para sumar o restar minutos a la hora dada.
	def sumaMin(self, Mm):
		self.M += Mm
		self.__init__(self.H,self.M)
	


#Ejecuci0n	de pruebas
pruebaUno = CalculoDeHora(8, 0)
print "Resultado esperado '08:00'"
pruebaDos = CalculoDeHora(72, 8640)
print "Resultado esperado '00:00'"
pruebaTres = CalculoDeHora(-1, 15)
print "Resultado esperado '23:15'"
pruebaCuatro = CalculoDeHora(-25, -160)
print "Resultado esperado '20:20'"
pruebaCinco = CalculoDeHora(10, 3).sumaMin(-70)
print "Resultado esperado '08:53'"